package org.nsu.fit.tm_backend.operations;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.nsu.fit.tm_backend.database.data.CustomerPojo;
import org.nsu.fit.tm_backend.database.data.SubscriptionPojo;
import org.nsu.fit.tm_backend.manager.CustomerManager;
import org.nsu.fit.tm_backend.manager.SubscriptionManager;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

public class StatisticOperationTest {
    private CustomerManager customerManager;
    private SubscriptionManager subscriptionManager;
    private List<UUID> customerIds;
    private CustomerPojo customer1;
    private CustomerPojo customer2;
    private List<SubscriptionPojo> subscriptions;

    @BeforeEach
    void init() {
        customerManager = mock(CustomerManager.class);
        subscriptionManager = mock(SubscriptionManager.class);

        customer1 = new CustomerPojo();
        customer1.id = UUID.randomUUID();
        customer1.balance = 100;
        customer2 = new CustomerPojo();
        customer2.id = UUID.randomUUID();
        customer2.balance = 200;

        customerIds = new ArrayList<>();
        customerIds.add(customer1.id);
        customerIds.add(customer2.id);

        subscriptions = new ArrayList<>();
        SubscriptionPojo subscriptionPojo1 = new SubscriptionPojo();
        subscriptionPojo1.planFee = 50;
        SubscriptionPojo subscriptionPojo2 = new SubscriptionPojo();
        subscriptionPojo2.planFee = 100;
        subscriptions.add(subscriptionPojo1);
        subscriptions.add(subscriptionPojo2);
    }

    @Test
    void ConstructorIllegalArguments() {
        Exception exception1 = assertThrows(IllegalArgumentException.class, () ->
                new StatisticOperation(null, subscriptionManager, customerIds));
        assertEquals("customerManager", exception1.getMessage());

        Exception exception2 = assertThrows(IllegalArgumentException.class, () ->
                new StatisticOperation(customerManager, null, customerIds));
        assertEquals("subscriptionManager", exception2.getMessage());

        Exception exception3 = assertThrows(IllegalArgumentException.class, () ->
                new StatisticOperation(customerManager, subscriptionManager, null));
        assertEquals("customerIds", exception3.getMessage());

        assertDoesNotThrow(() -> new StatisticOperation(customerManager, subscriptionManager, customerIds));
    }

    @Test
    void TestExecuteMethod() {
        when(customerManager.getCustomer(customer1.id)).thenReturn(customer1);
        when(customerManager.getCustomer(customer2.id)).thenReturn(customer2);
        when(subscriptionManager.getSubscriptions(any())).thenReturn(subscriptions);

        StatisticOperation statisticOperation = new StatisticOperation(customerManager, subscriptionManager, customerIds);
        StatisticOperation.StatisticOperationResult result = statisticOperation.Execute();
        assertEquals(result.overallBalance, customer1.balance + customer2.balance);
        assertEquals(result.overallFee, 2 * subscriptions.stream().map((s) -> s.planFee).reduce(0, Integer::sum));

        verify(customerManager, times(2)).getCustomer(any());
    }

}