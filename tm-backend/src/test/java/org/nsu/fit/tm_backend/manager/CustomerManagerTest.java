package org.nsu.fit.tm_backend.manager;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.nsu.fit.tm_backend.database.IDBService;
import org.nsu.fit.tm_backend.database.data.ContactPojo;
import org.nsu.fit.tm_backend.database.data.CustomerPojo;
import org.nsu.fit.tm_backend.database.data.TopUpBalancePojo;
import org.nsu.fit.tm_backend.manager.auth.data.AuthenticatedUserDetails;
import org.nsu.fit.tm_backend.shared.Authority;
import org.nsu.fit.tm_backend.shared.Globals;
import org.slf4j.Logger;

import java.util.Collections;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

// Лабораторная 2: покрыть юнит тестами класс CustomerManager на 100%.
class CustomerManagerTest {
    private Logger logger;
    private IDBService dbService;
    private CustomerManager customerManager;

    private CustomerPojo createCustomerInput;
    private CustomerPojo createCustomerOutput;

    @BeforeEach
    void init() {
        // Создаем mock объекты.
        dbService = mock(IDBService.class);
        logger = mock(Logger.class);

        // Создаем класс, методы которого будем тестировать,
        // и передаем ему наши mock объекты.
        customerManager = new CustomerManager(dbService, logger);

        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "John";
        createCustomerInput.lastName = "Wick";
        createCustomerInput.login = "john_wick@example.com";
        createCustomerInput.pass = "Baba_Jaga";
        createCustomerInput.balance = 0;

        createCustomerOutput = new CustomerPojo();
        createCustomerOutput.id = UUID.randomUUID();
        createCustomerOutput.firstName = "John";
        createCustomerOutput.lastName = "Wick";
        createCustomerOutput.login = "john_wick@example.com";
        createCustomerOutput.pass = "Baba_Jaga";
        createCustomerOutput.balance = 0;
    }

    @Test
    void testCreateCustomer() {
        when(dbService.createCustomer(createCustomerInput)).thenReturn(createCustomerOutput);
        when(dbService.getCustomerByLogin(createCustomerInput.login)).thenThrow(new IllegalArgumentException());

        // Вызываем метод, который хотим протестировать
        CustomerPojo customer = customerManager.createCustomer(createCustomerInput);

        // Проверяем результат выполенния метода
        assertEquals(customer.id, createCustomerOutput.id);

        // Проверяем, что метод по созданию Customer был вызван ровно 1 раз с определенными аргументами
        verify(dbService, times(1)).createCustomer(createCustomerInput);

        // Проверяем, что другие методы не вызывались...
        verify(dbService, times(0)).getCustomers();
    }

    @Test
    void testCreateCustomerWithNullArgument() {
        Exception exception = assertThrows(IllegalArgumentException.class, () ->
                customerManager.createCustomer(null));
        assertEquals("Argument 'customer' is null.", exception.getMessage());
    }

    @Test
    void testCreateCustomerWithNullPass() {
        createCustomerInput.pass = null;
        Exception exception = assertThrows(IllegalArgumentException.class, () ->
                customerManager.createCustomer(createCustomerInput));
        assertEquals("Field 'customer.pass' is null.", exception.getMessage());
    }

    @Test
    void testCreateCustomerLengthPassword() {
        createCustomerInput.pass = "12345";
        Exception exception1 = assertThrows(IllegalArgumentException.class, () -> customerManager.createCustomer(createCustomerInput));
        assertEquals("Password's length should be more or equal 6 symbols and less or equal 12 symbols.", exception1.getMessage());

        createCustomerInput.pass = "1234567890123";
        Exception exception = assertThrows(IllegalArgumentException.class, () -> customerManager.createCustomer(createCustomerInput));
        assertEquals("Password's length should be more or equal 6 symbols and less or equal 12 symbols.", exception.getMessage());

        when(dbService.getCustomerByLogin(createCustomerInput.login)).thenThrow(new IllegalArgumentException());

        createCustomerInput.pass = "123456";
        createCustomerOutput.pass = createCustomerInput.pass;
        when(dbService.createCustomer(createCustomerInput)).thenReturn(createCustomerOutput);
        assertDoesNotThrow(() -> customerManager.createCustomer(createCustomerInput));

        createCustomerInput.pass = "123456789012";
        createCustomerOutput.pass = createCustomerInput.pass;
        when(dbService.createCustomer(createCustomerInput)).thenReturn(createCustomerOutput);
        assertDoesNotThrow(() -> customerManager.createCustomer(createCustomerInput));

        createCustomerInput.pass = "1234567";
        createCustomerOutput.pass = createCustomerInput.pass;
        when(dbService.createCustomer(createCustomerInput)).thenReturn(createCustomerOutput);
        assertDoesNotThrow(() -> customerManager.createCustomer(createCustomerInput));

        createCustomerInput.pass = "12345678901";
        createCustomerOutput.pass = createCustomerInput.pass;
        when(dbService.createCustomer(createCustomerInput)).thenReturn(createCustomerOutput);
        assertDoesNotThrow(() -> customerManager.createCustomer(createCustomerInput));
    }

    @Test
    void testCreateCustomerWithEasyPassword() {
        createCustomerInput.pass = "123qwe";

        Exception exception1 = assertThrows(IllegalArgumentException.class, () -> customerManager.createCustomer(createCustomerInput));
        assertEquals("Password is very easy.", exception1.getMessage());

        createCustomerInput.pass = "1q2w3e";

        Exception exception2 = assertThrows(IllegalArgumentException.class, () -> customerManager.createCustomer(createCustomerInput));
        assertEquals("Password is very easy.", exception2.getMessage());
    }

    @Test
    void testCreateCustomerWithNotZeroBalance() {
        createCustomerInput.balance = 1;
        Exception exception = assertThrows(IllegalArgumentException.class, () ->
                customerManager.createCustomer(createCustomerInput));
        assertEquals("Balance must be 0.", exception.getMessage());
    }

    @Test
    void testCreateCustomerWithExistLogin() {
        when(dbService.getCustomerByLogin(createCustomerInput.login)).thenReturn(createCustomerInput);
        Exception exception = assertThrows(IllegalArgumentException.class, () ->
                customerManager.createCustomer(createCustomerInput));
        assertEquals("Customer with this login already exist.", exception.getMessage());
    }

    @Test
    void testGetCustomers() {
        when(dbService.getCustomers()).thenReturn(Collections.emptyList());
        assertEquals(customerManager.getCustomers(), Collections.emptyList());
    }

    @Test
    void testGetCustomer() {
        when(dbService.getCustomer(any())).thenReturn(createCustomerOutput);
        assertEquals(customerManager.getCustomer(createCustomerOutput.id), createCustomerOutput);
    }

    @Test
    void testLookupCustomer() {
        when(dbService.getCustomers()).thenReturn(Collections.singletonList(createCustomerOutput));
        assertEquals(customerManager.lookupCustomer(createCustomerOutput.login), createCustomerOutput);
        assertNull(customerManager.lookupCustomer(""));
    }

    @Test
    void testDeleteCustomer() {
        assertDoesNotThrow(() -> customerManager.deleteCustomer(createCustomerOutput.id));
    }

    @Test
    void testTopUpBalance() {
        TopUpBalancePojo topUpBalancePojo = new TopUpBalancePojo();
        topUpBalancePojo.customerId = createCustomerOutput.id;
        topUpBalancePojo.money = 100;

        when(dbService.getCustomer(topUpBalancePojo.customerId)).thenReturn(createCustomerOutput);

        createCustomerOutput.balance += topUpBalancePojo.money;
        assertEquals(customerManager.topUpBalance(topUpBalancePojo), createCustomerOutput);
    }

    @Test
    void testMe() {
        AuthenticatedUserDetails admin = new AuthenticatedUserDetails("1", "test", Collections.singleton(Authority.ADMIN_ROLE));

        ContactPojo contactPojo = new ContactPojo();
        contactPojo.login = Globals.ADMIN_LOGIN;
        assertEquals(customerManager.me(admin).login, contactPojo.login);

        AuthenticatedUserDetails user = new AuthenticatedUserDetails("1", "test", Collections.emptySet());
        when(dbService.getCustomerByLogin(user.getName())).thenReturn(createCustomerOutput);
        assertEquals(customerManager.me(user), createCustomerOutput);
        assertTrue(customerManager.me(user) instanceof CustomerPojo);
    }
}
