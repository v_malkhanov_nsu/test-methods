package org.nsu.fit.tests.api;

import org.nsu.fit.services.rest.RestClient;
import org.nsu.fit.services.rest.data.AccountTokenPojo;
import org.nsu.fit.services.rest.data.PlanPojo;
import org.nsu.fit.services.rest.data.SubscriptionPojo;

import java.util.UUID;

public abstract class BaseTest {
    public AccountTokenPojo token;
    public RestClient restClient = new RestClient();
    public PlanPojo plan = new PlanPojo();

    BaseTest() {
        plan.name = "test_plan";
        plan.fee = 100;
        plan.details = "test_details";
    }

    public void authAsAdmin() {
        token = restClient.authenticate("admin", "setup");
    }

    public void auth(String login, String pass) {
        token = restClient.authenticate(login, pass);
    }

    public PlanPojo createPlan() {
        return restClient.createPlan(token, plan);
    }

    public SubscriptionPojo newSubscription(PlanPojo plan, UUID customerId) {
        SubscriptionPojo subscription = new SubscriptionPojo();
        subscription.planId = plan.id;
        subscription.customerId = customerId;
        subscription.planDetails = plan.details;
        subscription.planFee = plan.fee;
        subscription.planName = plan.name;
        return subscription;
    }
}
