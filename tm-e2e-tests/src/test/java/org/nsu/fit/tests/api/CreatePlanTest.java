package org.nsu.fit.tests.api;

import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.nsu.fit.services.rest.data.PlanPojo;
import org.testng.Assert;
import org.testng.annotations.Test;

public class CreatePlanTest extends BaseTest {
    @Test(description = "Create plan.")
    @Severity(SeverityLevel.BLOCKER)
    @Feature("Plan feature.")
    public void createPlanTest() {
        authAsAdmin();

        PlanPojo actualPlan = createPlan();

        Assert.assertEquals(actualPlan.name, plan.name);
        Assert.assertEquals(actualPlan.fee, plan.fee);
        Assert.assertEquals(actualPlan.details, plan.details);
    }
}
