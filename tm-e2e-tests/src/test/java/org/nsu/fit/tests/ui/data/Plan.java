package org.nsu.fit.tests.ui.data;

public class Plan {
    public String name;
    public String details;
    public int fee;

    public Plan(String name, String details, int fee) {
        this.name = name;
        this.details = details;
        this.fee = fee;
    }
}
