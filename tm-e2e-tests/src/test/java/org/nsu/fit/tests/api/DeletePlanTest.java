package org.nsu.fit.tests.api;

import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.nsu.fit.services.rest.data.PlanPojo;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;

public class DeletePlanTest extends BaseTest {
    @Test(description = "Delete plan.")
    @Severity(SeverityLevel.BLOCKER)
    @Feature("Plan feature.")
    public void deletePlanTest() {
        authAsAdmin();

        PlanPojo actualPlan = createPlan();
        List<PlanPojo> plans = restClient.getPlans(token.id, token);

        Assert.assertTrue(plans.stream().anyMatch((p) -> p.id.equals(actualPlan.id)));

        restClient.deletePlan(token, actualPlan.id);
        plans = restClient.getPlans(token.id, token);

        Assert.assertFalse(plans.stream().anyMatch((p) -> p.id.equals(actualPlan.id)));
    }
}
